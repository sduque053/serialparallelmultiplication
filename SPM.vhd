library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity SPM is
	port(
		A 			: in  std_logic_vector( 7 downto 0);
		Bi,EN,CLK: in  std_logic;
		C			 : out std_logic_vector(15 downto 0)
	);
end entity;

architecture RTL of SPM is

	signal AS,BS,CS : std_logic_vector(7 downto 0);
	signal RD,RQ	 : std_logic_vector(8 downto 0);
	signal RQ2		 : std_logic_vector(6 downto 0);
	signal CC		 : std_logic;
	
	
	begin
	
	M1: for i in 0 to 7 generate
		BS(i)<=A(i) and Bi;
	end generate M1;
	
		
	Add: lpm_add_sub
		generic map(
			lpm_width				=>8,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>AS,
			datab		=>BS,
			result	=>CS,
			cout	   =>CC
		);
	
	RD<=CC&CS;
	
	REG1: lpm_ff
		generic map(
			lpm_width  =>9
		)
		port map(
			clock	=>CLK,
			enable=>EN,
			sload =>EN,
			data	=>RD,
			q		=>RQ
		);
		
	AS<=RQ(8 downto 1);
	
	REG2: lpm_shiftreg
		generic map(
			lpm_width 	  => 7,
			lpm_direction => "RIGHT"
		)
		port map(
			clock 	=> CLK,
			shiftin	=> RQ(0),
			enable	=> EN,
			q			=> RQ2
		);
		
	C<=RQ&RQ2;

	
	
	
end RTL;